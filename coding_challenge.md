# Coding Challenge Guidelines
## Functional Requirements
Prototype one of the projects defined below:

1. Hamster Blogging Platform (Fullstack Challenge)
2. Flickr Interesting Images (Frontend Challenge)
3. Email Service (Backend Challenge)

### Hamster Blogging Platform
Create a prototype of a blogging service for hamsters based on the following user stories:

* As a hamster, I want to sign up to a platform so I can connect with other the hamsters on the network
* As a social hamster, I want to be able to share posts with hamsters in the network
* As a social hamster I want the hamsters in the network to like my posts

Feel free to use social single sign-on services such as Facebook and Google.

### Flickr Interesting Images
Create a service that interacts with the Flickr public API based on the following user stories:

* As a User I want to be able to see a set of Interesting images so that I can get some visual inspiration (please use the flickr.interestingness.getList API for this purpose).
* As a User I want to be able to click on a specific image in the "interesting set" so that I can see a full version of the image and additional information about that image.

The Flickr API documentation can be found here: https://www.flickr.com/services/api/.

To use the API you will need to apply for an API key here: https://www.flickr.com/services/api/keys/apply/

### Email Service
(Thanks to Uber for this challenge)

Create a service that accepts the necessary information and sends emails. It should provide an abstraction between two different email service providers. If one of the services goes down, your service can quickly failover to a different provider without affecting your customers.

Example Email Providers:

* [SendGrid - Simple Send Documentation](https://sendgrid.com/docs/API_Reference/Web_API/mail.html)
* [Mailgun - Simple Send Documentation](http://documentation.mailgun.com/quickstart.html#sending-messages)
* [SparkPost - Developer Hub](https://www.sparkpost.com/)
* [Amazon SES - Simple Send Documentation](http://docs.aws.amazon.com/ses/latest/APIReference/API_SendEmail.html)

All listed services are free to try and are pretty painless to sign up for, so please register your own test accounts on each.


## Technical Specifications
The architecture will be split between a back-end and a web front-end, for instance providing a JSON in/out RESTful API. Feel free to use any other technologies provided that the general client/service architecture is respected.

Choose one of the following technical tracks that best suits your skillset:

1. **Full-stack**: include both front-end and back-end.
2. **Back-end track**: include a minimal front-end (e.g. a static view or API docs). Write, document and test your API as if it will be used by other services.
3. **Front-end track**: include a minimal back-end, or use the data service directly. Focus on making the interface as polished as possible.

### Back-end
At Adepto our back-end services are built on a combination of open source technologies. We expect that we will continue to invest in diversity as we grow, but for now the majority of the services are built in either JavaScript (Node.js) or Java. Both SQL (PostgreSQL) and noSQL (MongoDB) database technologies are used.

We would prefer that you use some combination of these technologies. However if you have a compelling reason to use something else, then also feel free to do this, either way please feel free to mention in your README how much experience you have with the technical stack you choose, we will take note of that when reviewing your challenge.

### Front-end
The front-end should ideally be a single page app based on the ES6 Javascript standard and you may take this opportunity to demonstrate your CSS3 or HTML5 knowledge. Please use a framework such as React.js and where sensible a state management framework such as MobX or Redux.

For reference the Adepto experience is delivered using React.js 16.4 with Material UI and Styled Components and MobX for state management.