# Adepto-Code-Challenge


## Overview
The Adepto [code challenge](./coding_challenge.md) is an opportunity for you to demonstrate the problem solving, software design and engineering skills we expect you to use day in and day out at Adepto. The aim of this exercise is to simulate real working conditions to provide context for a code/design review during the follow up technical interview.

We respect your time and If you already have existing code that you are particularly proud of and would prefer to share this with us then that is ok as well.

## Our Expectations

### Existing Code
If you have existing code, please follow the following guidelines:

* Include a link to the hosted repository (e.g. Github, Bitbucket...). We cannot review archives or single files.
* The repo should include a README that follows the principles described below In particular, please make sure to include high-level explanation about what the code is doing.
* Ideally, the code you're providing:
  * Has been written by you alone. If not, please tell us which part you wrote and are most proud of in the README.
  * Is leveraging web technologies.
  * Is deployed and hosted somewhere.

### Doing the Challenge and Time Allotment
Select one of the [specified challenges](./coding_challenge.md) that best suits your skills and the role that has been described to you and create a new public repository for that project (again either Git or Bitbucket is fine). 

You are free to complete the challenge in one sitting, or spread the work across a couple of sessions. We want to get a sense of your thought process and development patterns. If there are features you don't have time to implement, feel free to use pseudo code to describe the intended behaviour, and identify things that have been left out in your project's README.


### The Readme.
Regardless of whether it's your own code or our coding challenge, your README should include the following items:

* Description of the problem and solution.
* Reasoning behind your technical and architectural choices. Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the project.
* Link to other code you're particularly proud of.
* Link to to the hosted application where applicable.

## Technical Stack
Below is our daily technical stack that we work with so we would like to see what you can come up with using the same stack. If you can incorporate the optional points that would be preferable but not essential.

##### Frontend
- Latest version of React
- MobX State Tree or Redux
- Material UI and Styled Components (optional) 
 
##### Backend
- NodeJS or Java
- MongoDB or Postgres


## What we Review
Your application/code will be reviewed by a number of our engineers, the exact mix will depend on the challenge that you have attempted, and/or the style of application that you have submitted. We will take into account your level and years of experience during the review, but regardless our review will focus on the following:

We value **quality** over **feature-completeness**. It is fine to leave things aside provided you call them out in your project's README.

The aspects of your code we will assess include:

* **Code quality**: is the code simple, easy to understand, and maintainable? Are there any code smells or other red flags? Does object-oriented code follows principles such as the single responsibility principle? Is the coding style consistent with the language's guidelines? Is it consistent throughout the codebase?
* **Clarity**: does the README clearly and concisely explains the problem and solution? Are technical tradeoffs explained?
* **Correctness**: does the application do what was asked? If there is anything missing, does the README explain why it is missing?
* **Security**: are there any obvious vulnerability?
* **Testing**: how thorough are the automated tests? Will they be difficult to change if the requirements of the application were to change? Are there some unit and some integration tests?
We're not looking for full coverage (given time constraint) but just trying to get a feel for your testing skills.
* **Architecture**: if appropriate how clean is the separation between the front-end and the back-end?
* **UX**: if the solution includes a web interface is it understandable and pleasing to use? If the solution is primarily an API is it intuitive?
* **Technical choices**: do choices of libraries, databases, architecture etc. seem appropriate for the chosen application?


# Code Challenge
[The guidelines for the challenges can be found here.](./coding_challenge.md)
